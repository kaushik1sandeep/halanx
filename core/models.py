from django.db import models
from django.contrib.auth.models import AbstractUser
# Create your models here.
from django.utils.safestring import mark_safe
from django.dispatch import receiver
from django.db.models.signals import post_save
from rest_framework.authtoken.models import Token


class Address(models.Model):
    street_address=models.CharField(max_length=100)
    city=models.CharField(max_length=100)
    state=models.CharField(max_length=100)
    pincode=models.IntegerField()
    country=models.CharField(max_length=100)
    def __str__(self):
        return '{}, {}, {}, {}, {}'.format(self.street_address,self.city,self.pincode,self.state,self.country)


class Profile(AbstractUser):
    choices=(('male','male'),('female','female'))
    phone= models.IntegerField(unique=True,null=True)
    gender=models.CharField(choices=choices, max_length=10)
    profile_pic=models.ImageField(null=True)
    date_of_birth=models.DateField(null=True)
    permanent_address=models.OneToOneField(Address,on_delete=models.CASCADE,related_name='permanent_address',null=True)
    company_address=models.OneToOneField(Address,on_delete=models.CASCADE,related_name='company_address',null=True)
    friends =models.ManyToManyField('Profile', blank=True)

    def image_tag(self):
        if self.profile_pic:
            return mark_safe('<img src="%s" style="width: 45px; height:45px;" />' % self.profile_pic.url)
        else:
            return 'No Image Found'
    image_tag.short_description = 'Image'


    def __str__(self):
        return self.username

@receiver(post_save, sender=Profile)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)

