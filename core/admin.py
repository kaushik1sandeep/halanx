from django.contrib import admin
from core.models import *
# Register your models here.


class CustomModelAdmin(admin.ModelAdmin):
    search_fields = ('username',)
    list_display = ('username','phone','gender','image_tag','permanent_address')
    list_filter = ('gender','permanent_address__city')

class CustomAdmin(admin.AdminSite):
    pass


customAdminSite=CustomAdmin()
customAdminSite.register(Profile,CustomModelAdmin)
customAdminSite.register(Address)


