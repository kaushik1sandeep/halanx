from django.shortcuts import render
from rest_framework import viewsets
from core.models import *
from core.serializers import *
from rest_framework.authentication import TokenAuthentication,SessionAuthentication,BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import exceptions
from rest_framework import authentication
from django.contrib.auth import authenticate, get_user_model
from django.utils.translation import  ugettext_lazy as _



# Create your views here.








# ________________________________________________________________________________-Viewsest


class Logout(APIView):
    def get(self, request, format=None):
        # simply delete the token to force a login
        request.user.auth_token.delete()
        return Response(status=status.HTTP_200_OK)


class ExampleAuthentication(authentication.BaseAuthentication):

    def authenticate(self, request):

        # Get the username and password
        username = request.data.get('username', None)
        password = request.data.get('password', None)
        print(username)
        print(password)
        print(request.data)
        print(not username)
        print(not password)
        if not username or not password:
            raise exceptions.AuthenticationFailed(_('No credentials provided.'))

        credentials = {
            get_user_model().USERNAME_FIELD: username,
            'password': password
        }

        user = authenticate(**credentials)
        print(user)

        if user is None:
            raise exceptions.AuthenticationFailed(_('Invalid username/password.'))

        if not user.is_active:
            raise exceptions.AuthenticationFailed(_('User inactive or deleted.'))



class MyView(APIView):
    authentication_classes = ( ExampleAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, format=None):

        print("in post")
        content = {
            'user': request.user,
            'auth': request.auth,  # None
        }
        print(content)
        return Response(content)

class ProfileViewSet(viewsets.ModelViewSet):

    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer
    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAuthenticated,)


    def perform_create(self, serializer):
        serializer.save()
