from django.urls import path,include
from core.views import *
from rest_framework.routers import DefaultRouter
from rest_framework.authtoken import views
# from django.contrib.auth.views import login
router=DefaultRouter()

router.register('profile',ProfileViewSet,'profile')
urlpatterns = [
    path('api/', include(router.urls),name='api'),
    path('api-token-auth/', views.obtain_auth_token, name='api-token-auth'),
    path('stuff/', MyView.as_view()),
]