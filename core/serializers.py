from rest_framework import serializers
from core.models import *
class ProfileSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True)

    class Meta:
        model = Profile
        fields = ('username','first_name', 'last_name', 'email', 'phone', 'password')

    def create(self, validated_data):
        print("in create method")
        user = super(ProfileSerializer, self).create(validated_data)
        user.set_password(validated_data['password'])
        user.save()
        return user


    def validate(self, attrs):
        print("in validate method")
        print(attrs)
        return super(ProfileSerializer, self).validate(attrs)



